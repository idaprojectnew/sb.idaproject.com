#СБ Девелопмент

Перед началом работы нужно установить зависимости:
```bash
npm install
```
Удобнее через [Yarn](https://yarnpkg.com):
```bash
yarn
```

##Режимы
Одноразовая сборка:
```bash
npm start
```

Запуск живой сборки на локальном сервере:
```bash
npm run live
```

При `live` режиме страница автоматически перезагружается после изменения файла шаблона, стилей или скрипта.

##Структура проекта
```
sb.idaproject.com/
├── app – корень сайта, сервер смотрит в эту папку. 
├    └── assets – готовые файлы
├── source – исходные файлы проекта
├── templates – шаблоны и данные
├── gulpfile.js
└── package.json
```


##Шаблонизация
Шаблоны собираются из папки `templates` с помощью [twig](http://twig.sensiolabs.org/). Составные части лежат в `blocks`. Боевые файлы автоматически собираются в папке `app` – корне проекта.

##Данные
Данные можно хранить в JSON и передавать в шаблон с помощью [gulp-data](https://github.com/colynb/gulp-data/). JSON файлы берутся по названию шаблона, например: данные из `index.twig.json` передаются в шаблон `index.twig`.

##Стили
Компилируются из `source/styles/main.pcss` в `app/assets/css/style.css`.

- вложенность ([postcss-nested](https://github.com/postcss/postcss-nested))
- импорт ([postcss-import](https://github.com/postcss/postcss-import))
- CSS4 ([postcss-cssnext](http://cssnext.io)) 

##Картинки
Копируются из `source/images/` в `app/assets/images/`.

##Шрифты
Копируются из `source/fonts/` в `app/assets/fonts/`. Можно использовать любые шрифты в формате `woff` или `woff2`. Этот формат [поддерживают все современные браузеры (IE9+)](http://caniuse.com/#feat=woff).

##Скрипты
Компилируются из `source/scripts/main.js` в `app/assets/js/scripts.js`.

- [ES2015](https://babeljs.io/learn-es2015/) — используется [Babel](https://babeljs.io/).
- [Модули](https://learn.javascript.ru/modules) — используется [Browserify](http://browserify.org/).
- Линтинг — используется [ESLint](http://eslint.org/).

##Авторы
Используется собственный форк репозитория [Инитум](https://github.com/straykov/initium).