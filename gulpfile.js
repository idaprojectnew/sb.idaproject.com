'use strict';

var gulp = require('gulp'),
  del = require('del'),
  rename = require('gulp-rename'),
  gutil = require('gulp-util'),
  plumber = require('gulp-plumber'),
  portfinder = require('portfinder'),
  browserSync = require("browser-sync"),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  twig = require('gulp-twig'),
  data = require('gulp-data'),
  path = require('path'),
  fs = require('fs'),
  svgo = require('gulp-svgo'),

  nano = require('gulp-cssnano'),
  postcss = require('gulp-postcss'),
  nested = require('postcss-nested'),
  cssnext = require('postcss-cssnext'),
  imprt = require('postcss-import'),

  eslint = require('gulp-eslint'),
  babel = require("gulp-babel"),
  babelify = require('babelify'),
  watchify = require('watchify'),
  browserify = require('browserify'),
  source = require('vinyl-source-stream'),
  assign = require('lodash.assign'),
  streamify = require('gulp-streamify');

var processors = [
  imprt(),
  cssnext(),
  nested(),
];

// Ресурсы проекта
var paths = {
  styles: 'source/styles/',
  css: 'app/assets/css/',
  scripts: 'source/scripts/',
  js: 'app/assets/js/',
  templates: 'templates/',
  html: 'app/',
  fonts_src: 'source/fonts/',
  fonts_dest: 'app/assets/fonts/',
  images_src: 'source/images/',
  images_dest: 'app/assets/images/',
};

// Одноразовая сборка проекта
gulp.task('default', ['without-scripts', 'scripts'], function() {});

// Сборка проекта без скриптов
gulp.task('without-scripts', function() {
  gulp.start('twig', 'styles', 'fonts', 'images');
});

// Запуск живой сборки
gulp.task('live', function() {
  gulp.start('server');
});

// Запуск туннеля в интернет
gulp.task('external-world', function() {
  gulp.start('default', 'watch', 'web-server');
});

// Федеральная служба по контролю за оборотом файлов
gulp.task('watch', function() {
  gulp.watch(paths.templates + '**/*.twig', ['twig']);
  gulp.watch(paths.styles + '**/*.{pcss,scss}', ['styles']);
  gulp.watch(paths.images_src + '*.{png,jpg,gif,svg}', ['images']);
  gulp.watch(paths.scripts + '*.js', ['lint']);
});

// Шаблонизация
gulp.task('twig', function() {
  gulp.src(paths.templates + '*.twig')
    .pipe(plumber({errorHandler: onError}))
    .pipe(data(function(file) {
      var pathToJson = './templates/' + path.basename(file.path) + '.json';
      if (fs.existsSync(pathToJson)) {
        return JSON.parse(fs.readFileSync(pathToJson));
      }
    }))
    .pipe(twig())
    .pipe(gulp.dest(paths.html))
    .pipe(browserSync.stream());
});

// Компиляция стилей, добавление префиксов
gulp.task('styles', function () {
  gulp.src(paths.styles + 'main.pcss')
    .pipe(plumber({errorHandler: onError}))
    .pipe(postcss(processors))
    .pipe(rename('style.css'))
    .pipe(nano({convertValues: {length: false}}))
    .pipe(gulp.dest(paths.css))
    .pipe(browserSync.stream());
});

// Линтинг
gulp.task('lint', function() {
  return gulp.src(paths.scripts + '*.js')
    .pipe(plumber({errorHandler: onError}))
    .pipe(eslint())
    .pipe(eslint.format());
});

// Сборка и минификация скриптов
gulp.task('scripts', ['lint'], function() {
  return browserify(paths.scripts + 'main.js')
    .transform(babelify.configure({
      presets : ["es2015"]
    }))
    .bundle()
    .pipe(plumber({errorHandler: onError}))
    .pipe(source("scripts.js"))
    .pipe(gulp.dest(paths.js))
    .pipe(browserSync.stream());
});

// Отдельный вотчер за скриптами
gulp.task('watchify', function() {
  var customOpts = {
    entries: paths.scripts + 'main.js',
  };
  var opts = assign({}, watchify.args, customOpts);
  var b = watchify(browserify(opts));
  b.transform(babelify.configure({
    presets : ["es2015"]
  }));
  return b
    .on('log', gutil.log)
    .on('update', function() {
      gutil.log('updating js...');
      b.bundle()
        .pipe(plumber({errorHandler: onError}))
        .pipe(source("scripts.js"))
        .pipe(gulp.dest(paths.js))
        .pipe(browserSync.stream());
    })
    .bundle()
    .pipe(plumber({errorHandler: onError}))
    .pipe(source("scripts.js"))
    .pipe(gulp.dest(paths.js))
    .pipe(browserSync.stream());
});

// Копирование шрифтов
gulp.task('fonts', function() {
  gulp.src(paths.fonts_src + '*.{woff,woff2}')
    .pipe(gulp.dest(paths.fonts_dest));
});

// Копирование изображений
gulp.task('images', function(){
  gulp.src(paths.images_src + '*/*.{png,jpg,gif}')
    .pipe(gulp.dest(paths.images_dest));
  gulp.src(paths.images_src + '*.svg')
      .pipe(svgo())
      .pipe(gulp.dest(paths.images_dest));
});

// svg optimizing
// gulp.task('svgo', function() {
//
//         .pipe(svgo())
//         .pipe(gulp.dest('app/assets/images'));
// });

// Локальный сервер
gulp.task('server', ['without-scripts', 'watch', 'watchify'], function() {
  portfinder.getPort(function (err, port) {
    browserSync({
      server: {
        baseDir: "app"
      },
      host: 'localhost',
      notify: false,
      port: port
    });
  });
});

// Локальный сервер c туннелем в интернет
gulp.task('web-server', function() {
  portfinder.getPort(function (err, port) {
    browserSync({
      server: {
        baseDir: "."
      },
      tunnel: true,
      host: 'localhost',
      notify: false,
      port: port
    });
  });
});

// Ошибки
var onError = function(error) {
  gutil.log([
    (error.name + ' in ' + error.plugin).bold.red,
    '',
    error.message,
    ''
  ].join('\n'));
  gutil.beep();
  this.emit('end');
};
