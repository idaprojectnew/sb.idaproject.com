/* An InfoBox is like an info window, but it displays
 * under the marker, opens quicker, and has flexible styling.
 * @param {GLatLng} latlng Point to place bar at
 * @param {Map} map The map on which to display this InfoBox.
 * @param {Object} opts Passes configuration options - content,
 *   offsetVertical, offsetHorizontal, className, height, width
 */
export default function InfoBox(latlng, map, opts) {
  google.maps.OverlayView.call(this);
  this.latlng_ = latlng;
  this.content_ = opts.content || "Hello World";
  this.offsetVertical_ = opts.offsetVertical || -185;
  this.offsetHorizontal_ = opts.offsetHorizontal || -5;

  this.height_ = opts.height || 175;
  this.width_ = opts.width || 270;
  this.backgroundImage = opts.backgroundImage || "url('http://ace.imageg.net/images/WIZ_ACE_myStore/mapBubble.png')";
  this.backgroundSize = opts.backgroundSize || 'center';
  this.map_ = map;

  // Once the properties of this OverlayView are initialized, set its map so
  // that we can display it.  This will trigger calls to panes_changed and
  // draw.
  this.setMap(this.map_);
}

/* InfoBox extends GOverlay class from the Google Maps API
 */
InfoBox.prototype = new google.maps.OverlayView();

/* Creates the DIV representing this InfoBox
 * @param {GMap2} map The map to add infobox to
 */
InfoBox.prototype.panes_changed = function() {
  if (this.div_) {
    this.div_.parentNode.removeChild(this.div_);
    this.div_ = null;
  }

  const panes = this.get('panes');

  if (panes) {
    // Create the DIV representing our Bar
    const div = this.div_ = document.createElement("div");
    div.style.border = "0px none";
    div.style.position = "absolute";
    div.style.backgroundImage = this.backgroundImage;
    div.style.backgroundSize = this.backgroundSize;
    div.style.paddingTop = "12px";
    div.style.width = this.width_ + "px";
    div.style.height = this.height_ + "px";
    const contentDiv = document.createElement("div");
    contentDiv.innerHTML = this.content_;
    const arrowDiv = document.createElement("div");
    arrowDiv.className = "infobox__arrow";
    const gradientDiv = document.createElement("div");
    gradientDiv.className = "infobox__gradient";
    const topDiv = document.createElement("div");
    topDiv.style.textAlign = "right";
    topDiv.style.paddingRight = "10px";

    div.appendChild(topDiv);
    div.appendChild(gradientDiv);
    div.appendChild(contentDiv);
    div.appendChild(arrowDiv);

    this.draw();

    // Then add this overlay to the DOM
    panes.floatPane.appendChild(div);
  }
};

/* Redraw the Bar based on the current projection and zoom level
 */
InfoBox.prototype.draw = function() {
  if (!this.div_) return;

  // Calculate the DIV coordinates of two opposite corners of our bounds to
  // get the size and position of our Bar
  const pixPosition = this.getProjection().fromLatLngToDivPixel(this.latlng_);
  if (!pixPosition) return;

  // Now position our DIV based on the DIV coordinates of our bounds
  this.div_.style.width = this.width_ + "px";
  this.div_.style.left = (pixPosition.x + this.offsetHorizontal_) + "px";
  this.div_.style.height = this.height_ + "px";
  this.div_.style.top = (pixPosition.y + this.offsetVertical_) + "px";
  this.div_.style.display = 'none';

  // if we go beyond map, pan map
  const mapWidth = this.map_.getDiv().offsetWidth;
  const mapHeight = this.map_.getDiv().offsetHeight;
  const bounds = this.map_.getBounds();
  const boundsSpan = bounds.toSpan();
  const longSpan = boundsSpan.lng();
  const latSpan = boundsSpan.lat();
  const degWidth = (this.width_/mapWidth) * longSpan;
  const degHeight = (this.height_/mapHeight) * latSpan;

  if (this.latlng_.lng() + degWidth > bounds.getNorthEast().lng()) {
    // this.map_.setCenter(this.latlng_);
  }

  const bottompt = new google.maps.LatLng( (this.latlng_.lat() - degHeight), this.latlng_.lng());
  if (!bounds.contains(bottompt)) {
    // this.map_.setCenter(this.latlng_);
  }
};

InfoBox.prototype.open = function() {
  this.div_.style.display = 'block';
};

InfoBox.prototype.close = function() {
  this.div_.style.display = 'none';
};
