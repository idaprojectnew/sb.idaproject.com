window.jQuery = window.$ = require("jquery");
require('owl.carousel');
require('velocity-animate');
import MarkerClusterer from './markerclusterer';
import InfoBox from './infobox';
import Vivus from 'vivus';
import 'remodal';

const styles = [{"featureType":"all","elementType":"all","stylers":[{"lightness":"29"},{"invert_lightness":true},{"hue":"#008fff"},{"saturation":"-73"}]},{"featureType":"all","elementType":"labels","stylers":[{"saturation":"-72"}]},{"featureType":"administrative","elementType":"all","stylers":[{"lightness":"32"},{"weight":"0.42"}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":"-53"},{"saturation":"-66"}]},{"featureType":"landscape","elementType":"all","stylers":[{"lightness":"-86"},{"gamma":"1.13"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"hue":"#006dff"},{"lightness":"4"},{"gamma":"1.44"},{"saturation":"-67"}]},{"featureType":"landscape","elementType":"geometry.stroke","stylers":[{"lightness":"5"}]},{"featureType":"landscape","elementType":"labels.text.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"weight":"0.84"},{"gamma":"0.5"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"visibility":"off"},{"weight":"0.79"},{"gamma":"0.5"}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"simplified"},{"lightness":"-78"},{"saturation":"-91"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"color":"#ffffff"},{"lightness":"-69"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"lightness":"5"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"lightness":"10"},{"gamma":"1"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"lightness":"10"},{"saturation":"-100"}]},{"featureType":"transit","elementType":"all","stylers":[{"lightness":"-35"}]},{"featureType":"transit","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"saturation":"-97"},{"lightness":"-14"}]}];
/**
 * Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
 * param  iNumber Integer Число на основе которого нужно сформировать окончание
 * param  aEndings Array Массив слов или окончаний для чисел (1, 4, 5),
 *         например ['яблоко', 'яблока', 'яблок']
 * return String
 */
const getNumEnding = (iNumber, aEndings) => {
  let sEnding, i;
  iNumber = iNumber % 100;
  if (iNumber >= 11 && iNumber <= 19) {
    sEnding = aEndings[2];
  } else {
    i = iNumber % 10;
    switch (i) {
      case (1): sEnding = aEndings[0]; break;
      case (2):
      case (3):
      case (4): sEnding = aEndings[1]; break;
      default: sEnding = aEndings[2];
    }
  }
  return sEnding;
};

const mapAndProjects = () => {
  $('#projects_slider').owlCarousel({
    items: 3,
    dotsEach: true,
    nav: true,
    autoWidth: true,
  });
  const center = {lat: 56.309151, lng: 38.938238};
  const map = new google.maps.Map(document.getElementById('map'), {
    center,
    styles,
    scrollwheel: false,
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    zoom: 6,
    backgroundColor: 'black',
  });
  const $projects = $('.js-project');
  const projectsCount = $projects.length;
  const projectsEnding = getNumEnding(projectsCount, ['проект', 'проекта', 'проектов']);
  $('#projects-count').text(`${projectsCount} ${projectsEnding}`);
  const locations = $projects.map((i, el) => new Object({
    lat: $(el).data('lat'),
    lng: $(el).data('lng'),
    image: $(el).find('.js-project-image').css('background-image'),
    title: $(el).find('.js-project-title').text(),
    type: $(el).find('.js-project-type').text(),
  })).toArray();
  const icon = '/assets/images/marker.png';
  let infoBoxes = [];
  const markers = locations.map((opts, i) => {
    const content = `<div class="infobox__info"><p class="infobox__type">${opts.type}</p><p class="infobox__title">${opts.title}</p></div>`;
    const position = new google.maps.LatLng(opts.lat, opts.lng);
    infoBoxes[i] = new InfoBox(position, map, {
      content,
      width: 180,
      height: 180,
      backgroundImage: opts.image,
      backgroundSize: '200% 100%',
      offsetVertical: -230,
      offsetHorizontal: -90,
    });
    const marker = new google.maps.Marker({position, icon});
    marker.addListener('mouseover', function() {
      for (let j = 0; j < infoBoxes.length; j++) {
        infoBoxes[j].close();
      }
      infoBoxes[i].open();
    });
    marker.addListener('mouseout', function() {
      infoBoxes[i].close();
    });
    return marker;
  });
  const mcOptions = {styles: [{
    height: 42,
    backgroundColor: '#1025B6',
    width: 42,
    textSize: 30,
    textColor: 'white',
  },]};
  const markerCluster = new MarkerClusterer(map, markers, mcOptions);
  const showProjectOnMap = e => {
    const $el = $(e.target).closest('.js-project');
    const i = $el.closest('.owl-item').index();
    const position = {
      lat: $el.data('lat') + 0.005,
      lng: $el.data('lng'),
    };
    map.panTo(position);
    map.setZoom(14);
    setTimeout(() => {
      for (let j = 0; j < infoBoxes.length; j++) {
        infoBoxes[j].close();
      }
      infoBoxes[i].open();
    }, 100);
    $("#map").velocity("scroll", {duration: 400});
  };
  let mapIsBig = false;
  const showMoreMap = e => {
    $('#map').velocity({
      height: '880px'
    }, 500, [.68,-0.55,.26,1.55], function() {
      google.maps.event.trigger(map, "resize");
    });
    $('#more-map').text('Свернуть карту');
    mapIsBig = true;
  };
  const showLessMap = e => {
    $('#map').velocity({
      height: '530px'
    }, 500, [.68,-0.55,.26,1.55], function() {
      google.maps.event.trigger(map, "resize");
    });
    $('#more-map').text('Раскрыть карту');
    mapIsBig = false;
  };
  $projects.on('click', showProjectOnMap);
  $('#more-map').on('click', () => {
    mapIsBig ? showLessMap() : showMoreMap();
  });
};
const contacts = () => {
  const center = {lat: 55.776262, lng: 37.676765};
  const position = {lat: 55.776267, lng: 37.676666};
  const icon = '/assets/images/marker-contacts.png';
  const map = new google.maps.Map(document.getElementById('contacts-map'), {
    center,
    styles,
    scrollwheel: false,
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    zoom: 19,
    backgroundColor: 'black',
  });
  const marker = new google.maps.Marker({
    position,
    map,
    icon,
  });
};
const popup = () => {
  const onSuccess = message => {
    console.info(message);
    const $popup = $('.remodal-is-opened');
    $popup.find('.popup__form').hide();
    $popup.find('.popup__success').fadeIn();
  };
  const onSubmit = e => {
    e.preventDefault();
    const $form = $(e.target);
    const data = new FormData($form[0]);
    const url = '/mail.php';
    $.ajax({
      url,
      data,
      contentType: false,
      processData: false,
      type: 'POST',
      success: onSuccess,
    });
  };
  const writeUsForm = $('#form_writeus');

  $('[data-remodal-id=writeus]').remodal({hashTracking: false});

  writeUsForm.on('submit', onSubmit);
};

$(document).ready(function() {

  const drawSvgInSlider = e => {
      const $svg = $(e.target).find(`.slide:eq(${e.item.index})`).find('svg');
      $('.slide__svg').removeClass('is-active');
      $svg.addClass('is-active');
      new Vivus($svg[0], {
          type: 'async',
          duration: 100
      }).play();
  };

  let $circleDots;
  const startPosition = 0;
  const initializeCircle = () => {
    $circleDots = $('.js-dots').find('.owl-dot');
    $circleDots.each((index, dot) => {
      const iterationAngle = 30;
      const angle = 90 - (startPosition - index) * iterationAngle;
      const halfWidth = 400;
      $(dot).velocity({
        rotateZ: `${angle}deg`,
        translateX: `${halfWidth * -1}px`,
      });
      $(dot).on('click', e => {
        if ($(dot).hasClass('is-disabled'))
          return false;
      });
    });
  };

  const rotateCircle = e => {
    const $circle = $('#about_circle');
    const index = e.item.index;
    const iterationAngle = 30;
    const angle = (startPosition - index) * iterationAngle;
    $circle.velocity({
      rotateZ: `${angle}deg`
    },{
      begin: () => {$circleDots.addClass('is-disabled');},
      complete: () => {$circleDots.removeClass('is-disabled');},
    });
  };

  $('#intro_slider').owlCarousel({
    items: 1,
    dotsData: true,
    smartSpeed: 400,
    onTranslated: drawSvgInSlider,
  });

  $("#about_slider").owlCarousel({
    items: 1,
    dotsContainer: '.js-dots',
    dotsData: true,
    smartSpeed: 400,
    startPosition,
    onInitialized: initializeCircle,
    onTranslate: rotateCircle
  });

  if ( $('#map').length !== 0  ) {
      mapAndProjects();
  }
  contacts();
  popup();

  $('.tender__button').click(function () {
      $(this).parent('.tender').toggleClass('tender_active');
      // $(this).parent().siblings().removeClass('tender_active');
      $(this).parent('.tender').find('.tender__description').slideToggle();
      // $(this).parent('.tender').siblings().find('.tender__description').slideUp();
  });


});
