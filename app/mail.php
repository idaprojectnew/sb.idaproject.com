<?php
require_once('phpmailer/class.phpmailer.php');
require_once('phpmailer/class.smtp.php');

if ( !isset($_POST['name']) || !isset($_POST['message']) || !isset($_POST['email']) )
    exit('Error');

$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
$message = filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING);
$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);

$mail = new PHPMailer;

$mail->isSMTP();
$mail->CharSet = 'UTF-8';
$mail->SMTPAuth = true;
$mail->Host = 'smtp.yandex.ru';
$mail->Username = '';
$mail->Password = '';
// $mail->SMTPSecure = 'tls';
// $mail->Port = 587;
$mail->Port = 25;

$mail->setFrom('am@idaproject.com', 'СБ-Девелопмент');
$mail->addAddress('am@idaproject.com');

$mail->isHTML(true);

$mail->Subject = 'Новая письмо с сайта';
$mail->Body    = '<p>Имя: '.$name.'</p><p>Сообщение: '.$message.'</p><p>Электронная почта: '.$email.'</p>';

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'success';
}
